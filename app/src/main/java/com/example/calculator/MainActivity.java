package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView result,solution;
    MaterialButton but_c,but_openB,but_closeB;
    MaterialButton but_plus,but_minus,but_multiply,but_divide,but_equals;
    MaterialButton but_0,but_1,but_2,but_3,but_4,but_5,but_6,but_7,but_8,but_9;
    MaterialButton but_ac,but_dot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        solution = findViewById(R.id.solution);
        result = findViewById(R.id.result);

        assignId(but_c,R.id.but_c);
        assignId(but_openB,R.id.but_openB);
        assignId(but_closeB,R.id.but_closeB);
        assignId(but_plus,R.id.but_plus);
        assignId(but_minus,R.id.but_minus);
        assignId(but_multiply,R.id.but_multiply);
        assignId(but_divide,R.id.but_divide);
        assignId(but_equals,R.id.but_equals);
        assignId(but_0,R.id.but_0);
        assignId(but_1,R.id.but_1);
        assignId(but_2,R.id.but_2);
        assignId(but_3,R.id.but_3);
        assignId(but_4,R.id.but_4);
        assignId(but_5,R.id.but_5);
        assignId(but_6,R.id.but_6);
        assignId(but_7,R.id.but_7);
        assignId(but_8,R.id.but_8);
        assignId(but_9,R.id.but_9);
        assignId(but_ac,R.id.but_ac);
        assignId(but_dot,R.id.but_dot);
    }

    void assignId (MaterialButton btn,int id){
        btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();
        String calculate = solution.getText().toString();

        if(buttonText.equals("AC")) {
            solution.setText("");
            result.setText("0");
            return;
        }if (buttonText.equals("=")){
            solution.setText(result.getText());
            Intent in = new Intent(MainActivity.this, MainActivity2.class);
            in.putExtra("operasi",calculate);
            in.putExtra("hasil",result.getText());
            startActivity(in);
        }if (buttonText.equals("C")){
            calculate = calculate.substring(0,calculate.length()-1);
        }else{
            calculate = calculate+buttonText;
        }
        solution.setText(calculate);

        String finalResult = getresult(calculate);

        if(!finalResult.equals("Err")) {
            result.setText(finalResult);
        }
    }

    String getresult(String data){
        try{
            Context context = Context.enter();
            context.setOptimizationLevel(-1);
            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable,data,"JavaScript",1,null).toString();
            if (finalResult.endsWith(".0")){
                finalResult = finalResult.replace(".0"," ");
            }
            return finalResult;
        }catch(Exception e){
            return "err";
        }
    }
}