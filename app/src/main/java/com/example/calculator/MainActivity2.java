package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    TextView operasi1,hasil1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        operasi1 = findViewById(R.id.operasi1);
        hasil1 = findViewById(R.id.hasil1);

        Intent intent = getIntent();
        String operasi = intent.getStringExtra("operasi");
        String hasil = intent.getStringExtra("hasil");

        operasi1.setText(operasi);
        hasil1.setText(hasil);

    }
}